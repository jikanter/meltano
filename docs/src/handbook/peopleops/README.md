---
sidebarDepth: 2
---

# PeopleOps Handbook

## Hiring Process

We're currently using [Greenhouse](https://www.greenhouse.io/) to manage the hiring process and referrals. All employees should have an account.

### Adding New Roles

- Open an issue on the [administration](https://gitlab.com/meltano/administration/) using the `new-job-creation.md` template.
- Fill out necessary details. All sections are required unless they're marked optional. Having all of this information helps us get the job online and ensures all candidates are evaluated based on the same interview process. This is just an initial version of the hiring process -- it can always be iterated on.
- A Site Admin will add the role to Greenhouse once the issue has been filled out and approved.
- Site Admin will add the hiring manager as a job admin to this role.
- Hiring manager will be responsible for screening candidates, setting up interviews, and moving them through the pipeline.

### Referrals

- Log into Greenhouse
- Click the green "Add a Referral" button
- Fill out the candidate details and attach a copy of their resume and cover letter if you have them.

## Greenhouse Administration

### Adding New Users

Add users [here](https://app4.greenhouse.io/account/users?status=active). Hiring managers can be added as "Job Admin" for 
a specific role. Other users can be added as "Basic" users and not added to any roles. This will allow them to make referrals.

- **Basic User:** Can add referrals to open roles or manage interviews they've been assigned to.
- **Job admins:** Can control the interview process for a specific role.
- **Site Admins:** Can add new roles, see all candidate info, see and modify roles, and manage the job board settings.

### Configure Job Introductions/Conclusions

By default, all job posts have an introduction and a conclusion that tells the applicant about Meltano and our benefits.

To configure these:
- Visit the [Job Board configuration page](https://app4.greenhouse.io/jobboard)
- Click the three dot menu next to the job board you want to configure
- Select "Edit Post Settings"
- Change the introduction and conclusion as needed

Please note: These settings are per job board. If there are multiple job boards you'll need to update this setting for 
each one.
